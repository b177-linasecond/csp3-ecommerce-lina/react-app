import {BrowserRouter as Router} from 'react-router-dom'
import {Container} from 'react-bootstrap'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'
import Products from './pages/Products'
import Rings from './pages/Rings'
import Bracelets from './pages/Bracelets'
import Necklaces from './pages/Necklaces'
import Earrings from './pages/Earrings'
import AllProducts from './pages/AllProducts'
import AllRings from './pages/AllRings'
import AllNecklaces from './pages/AllNecklaces'
import AllBracelets from './pages/AllBracelets'
import AllEarrings from './pages/AllEarrings'
import AddProduct from './pages/AddProduct'
import Order from './pages/Order'
import AllOrders from './pages/AllOrders'
import ProductView from './components/ProductView'
import UpdateProduct from './components/UpdateProduct'
import PageNotFound from './components/PageNotFound'
import {Route, Routes} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
import {UserProvider} from './UserContext'
import {useState} from 'react'
import Footer from './components/Footer'
import './App.css'

function App() {
  const [user, setUser] = useState({
    id : null,
    isAdmin : null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/products" element={<Products />} />
            <Route exact path="/products/rings" element={<Rings />} />
            <Route exact path="/products/necklaces" element={<Necklaces />} />
            <Route exact path="/products/bracelets" element={<Bracelets />} />
            <Route exact path="/products/earrings" element={<Earrings />} />
            <Route exact path="/products/all" element={<AllProducts />} />
            <Route exact path="/products/allRings" element={<AllRings />} />
            <Route exact path="/products/allNecklaces" element={<AllNecklaces />} />
            <Route exact path="/products/allBracelets" element={<AllBracelets />} />
            <Route exact path="/products/allEarrings" element={<AllEarrings />} />
            <Route exact path="/products/create" element={<AddProduct />} />
            <Route exact path="/products/:productId" element={<ProductView />} />
            <Route exact path="/products/update/:productId" element={<UpdateProduct />} />
            <Route exact path="/orders" element={<Order />} />
            <Route exact path="/orders/all" element={<AllOrders />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="*" element={<PageNotFound />} />
          </Routes>
        </Container>
      <Footer />
      </Router>
    </UserProvider>
  )
}

export default App