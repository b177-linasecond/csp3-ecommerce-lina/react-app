import {Fragment, useState, useEffect, useContext} from 'react'
import {Routes, Route} from 'react-router-dom'
import UserContext from '../UserContext'
import OrderCard from '../components/OrderCard'
import PageNotFound from '../components/PageNotFound'
import {Row, Container} from 'react-bootstrap'

const Orders = () => {
	const {user} = useContext(UserContext)
	
	const [order, setOrder] = useState([])

	useEffect(() => {
		fetch('https://ecommerce-ytmf.onrender.com/users/orders', {
			headers : {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrder(data.map(order => {
					return (
						<OrderCard key={order._id} orderProp={order} />
					)
				})
			)
		})
	}, [])

	return (
		(user.isAdmin === false || user.id === null) ?
			<Routes>
				<Route exact path="*" element={<PageNotFound />} />
			</Routes>
		:
			<Fragment>
				<Container className="mt-5" fluid>
					<Row className="mt-3 mb-3 justify-content-around">{order}</Row>
				</Container>
			</Fragment>
	)
}

export default Orders