import {Form, Button, Container, Row, Col} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Navigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'

const Login = () => {
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	},
	[email, password])

	const retrieveUserDetails = (token) => {
		fetch('https://ecommerce-ytmf.onrender.com/details', {
			headers : {
				Authorization : `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id : data._id,
				isAdmin : data.isAdmin
			})
		})
	}

	const loginUser = () => {
		fetch('https://ecommerce-ytmf.onrender.com/login', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email.toLowerCase(),
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			localStorage.setItem('token', data.access)
			retrieveUserDetails(data.access)

			Swal.fire({
				title : "You are now logged in.",
				icon : "success"
			})
		})
	}

	const checkPassword = () => {
		fetch('https://ecommerce-ytmf.onrender.com/checkPassword', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email.toLowerCase(),
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				loginUser()
			}
			else {
				Swal.fire({
					title : "Password incorrect.",
					icon : "error",
					text : "Check your password and try again."
				})
			}
		})
	}

	const checkUser = () => {
		fetch('https://ecommerce-ytmf.onrender.com/checkEmail', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},body : JSON.stringify({
				email : email.toLowerCase()
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				return (
					Swal.fire({
						title : "Email is not registered.",
						icon : "error",
						text : "Please try another email."
					})
				)
				
			}
			else {
				checkPassword()
			}
		})
	}

	const validEmail = e => {
		e.preventDefault()
		fetch('https://ecommerce-ytmf.onrender.com/checkIfEmail', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email.toLowerCase()
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === false) {
				Swal.fire({
					title : "Not a valid email.",
					icon : "error",
					text : "Please enter valid email."
				})	
			}
			else {
				checkUser()
			}
		})
		setEmail('')
		setPassword('')
	}

	return (
		(user.id !== null) ?
			<Navigate to="/" />
		:
			<Container fluid>
				<Row className="justify-content-center align-items-center vh-100 m-0 p-0">
					<Col xs="10" md="4" align="center">
						<Form onSubmit={e => validEmail(e)}>
							<h2>Login</h2>

							<Form.Group className="mb-3" controlId="userEmail">
								<Form.Control type="email" placeholder="Email address" value={email} onChange={e => setEmail(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="password">
								<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required />
							</Form.Group>

							{ isActive ?
								<Button variant="primary" type="submit" id="submitBtn">
									Login
								</Button>
							:
								<Button variant="primary" type="submit" id="submitBtn" disabled>
									Login
								</Button>
							}

							<p></p>

							<p>If you're not yet registered. <Link className="links" to="/register">Sign up.</Link></p>
						</Form>				
					</Col>
				</Row>
			</Container>
	)
}

export default Login