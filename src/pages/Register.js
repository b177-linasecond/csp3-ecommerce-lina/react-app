import {Form, Button, Container, Row, Col} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'

const Register = () => {
	const {user} = useContext(UserContext)
	const history = useNavigate()

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [password1, setPassword1] = useState('')
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNo.length > 10 && email !== '' && password !== '' && password1 !== '') && (password === password1)) {
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	},
	[firstName, lastName, mobileNo, email, password, password1])

	const registerUser = () => {
		fetch('https://ecommerce-ytmf.onrender.com/register', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				mobileNo : mobileNo,
				email : email.toLowerCase(),
				password : password
			})
		})
		.then(res => res.json())
		.then(data => data)

		Swal.fire({
			title : "You are now registered.",
			icon : "success"
		})

		history("/login")
	}

	const checkUser = () => {
		
		fetch('https://ecommerce-ytmf.onrender.com/checkEmail', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},body : JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				mobileNo : mobileNo,
				email : email.toLowerCase(),
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === false) {
				return (
					Swal.fire({
						title : "Email is already registered.",
						icon : "error",
						text : "Please use another email."
					})
				)
				
			}
			else {
				registerUser()
			}
		})
	}

	const validEmail = e => {
		e.preventDefault()
		fetch('https://ecommerce-ytmf.onrender.com/checkIfEmail', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},body : JSON.stringify({
				email : email.toLowerCase()
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === false) {
				return (
					Swal.fire({
						title : "Not a valid email.",
						icon : "error",
						text : "Please enter valid email."
					})
				)
				
			}
			else {
				checkUser()
			}
		})
		setEmail('')
		setPassword('')
		setPassword1('')
	}

	return (
		(user.id !== null) ?
			<Navigate to="login" />
		:
			<Container fluid>
				<Row className="justify-content-center align-items-center vh-100 m-0 p-0">
					<Col xs="10" md="4" align="center">
						<Form onSubmit={e => validEmail(e)}>
							<h1>Register</h1>

							<Form.Group className="mb-3" controlId="userFName">
								<Form.Control type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userLName">
								<Form.Control type="text" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userMNumber">
								<Form.Control type="text" placeholder="11-digit Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userEmail">
								<Form.Control type="email" placeholder="Email Address" value={email} onChange={e => setEmail(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="password">
								<Form.Control type="password" placeholder="Create Password" value={password} onChange={e => setPassword(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="password1">
								<Form.Control type="password" placeholder="Verify Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
							</Form.Group>

							{ isActive ?
								<Button variant="primary" type="submit" id="submitBtn">
									Register
								</Button>
							:
								<Button variant="primary" type="submit" id="submitBtn" disabled>
									Register
								</Button>
							}

							<p></p>

							<p>Already registered? <Link className="links" as={Link} to="/login">Sign in.</Link></p>
						</Form>
					</Col>
				</Row>
			</Container>
	)
}

export default Register