import {Fragment, useEffect, useState} from 'react'
import OrderCard from '../components/OrderCard'
import {Row, Container} from 'react-bootstrap'

const Orders = () => {
	const [order, setOrder] = useState([])

	useEffect(() => {
		fetch('https://ecommerce-ytmf.onrender.com/users/myOrders', {
			headers : {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrder(data.map(order => {
					return (
						<OrderCard key={order._id} orderProp={order} />
					)
				})
			)
		})
	}, [])

	return (
		<Fragment>
			<Container className="mt-5" fluid>
				<Row className="mt-3 mb-3 my-0 justify-content-around">{order}</Row>
			</Container>
		</Fragment>
	)
}

export default Orders