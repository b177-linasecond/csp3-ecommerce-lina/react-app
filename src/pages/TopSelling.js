import {useEffect, useState} from 'react'
import TopSelling from '../components/TopSelling'
import {Row, Container} from 'react-bootstrap'

const TopBracelets = () => {
	const [product, setProducts] = useState([])

	useEffect(() => {
		fetch('https://ecommerce-ytmf.onrender.com/TopProducts')
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {
				return (
					<TopSelling key={product._id} productProp={product} />
				)
			})
			)
		})
	}, [])

	return (
		<Container className="mt-4 pt-3" fluid>

			<Row className="ml-3 justify-content-center TopText">Most Popular</Row>

			<Row className="justify-content-around m-5">{product}</Row>

		</Container>
	)
}

export default TopBracelets