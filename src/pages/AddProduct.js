import {Form, Button, Container, Row, Col} from 'react-bootstrap'
import {useState, useContext} from 'react'
import {useNavigate, Routes, Route} from 'react-router-dom'
import PageNotFound from '../components/PageNotFound'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

const AddProduct = () => {
	const {user} = useContext(UserContext)
	const history = useNavigate()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState()
	const [quantity, setQuantity] = useState()
	const [image, setImage] = useState()
	const [category, setCategory] = useState()

	const addProduct = () => {
		fetch('https://ecommerce-ytmf.onrender.com/products', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body : JSON.stringify({
				name : name,
				description : description,
				price : price,
				quantity : quantity,
				image : image,
				category : category
			})
		})
		.then(res => res.json())
		.then(data => data)

		Swal.fire({
			title : "Product is listed.",
			icon : "success"
		})

		history("/products/all")
	}
	
	return (
		(user.isAdmin === false || user.id === null) ?
			<Routes>
				<Route exact path="*" element={<PageNotFound />} />
			</Routes>
		:
			<Container fluid>
				<Row className="justify-content-center align-items-center vh-100 m-0 p-0">
				<Col xs="10" md="4" align="center">
					<Form onSubmit={e => addProduct(e)}>
						<Form.Group className="mb-3" controlId="productImage">
							<h6>Photos</h6>

							<Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} required />
						</Form.Group>

						<Form.Group className="mb-3" controlId="productName">
							<h6>Product Name</h6>

							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
						</Form.Group>

						<Form.Group className="mb-3" controlId="productDescription">
							<h6>Description</h6>

							<Form.Control as="textarea" rows={3} type="text" value={description} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>

						<Form.Group className="mb-3" controlId="productPrice">
							<h6>Price</h6>

							<Form.Control type="number"  value={price} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>

						<Form.Group className="mb-3" controlId="stock">
							<h6>Stock</h6>

							<Form.Control type="number"  value={quantity} onChange={e => setQuantity(e.target.value)} required />
						</Form.Group>

						<Form.Group className="mb-3" controlId="category">
							<h6>Catgeory</h6>

							<Form.Select aria-label="Default select example" onChange={e => setCategory(e.target.value)} required >
								<option>Choose category</option>
								<option value="Ring">Ring</option>
								<option value="Necklace">Necklace</option>
								<option value="Bracelet">Bracelet</option>
								<option value="Earrings">Earrings</option>
							</Form.Select>
						</Form.Group>

						<Button variant="primary" type="submit" id="submitBtn">
							Add Product
						</Button>
					</Form>
				</Col>
				</Row>
			</Container>
	)
}

export default AddProduct