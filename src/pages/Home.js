import {Fragment} from 'react'
import Banner from '../components/Banner'
import TopSelling from './TopSelling'
import NewProduct from './NewProduct'

const Home = () => {
	return (
		<Fragment>
			<Banner />
			<NewProduct />
			<TopSelling />
		</Fragment>
	)
}

export default Home