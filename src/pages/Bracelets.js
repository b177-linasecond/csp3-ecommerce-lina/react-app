import {Fragment, useState, useEffect} from 'react'
import ProductCard from '../components/ProductCard'
import {Row, Container} from 'react-bootstrap'

const Bracelets = () => {
	const [product, setProducts] = useState([])

	useEffect(() => {
		fetch('https://ecommerce-ytmf.onrender.com/Bracelets')
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {
					return (
						<ProductCard key={product._id} productProp={product} />
					)
				})
			)
		})
	}, [])

	return (
		<Fragment>
			<Container className="my-3" fluid>
				<Row className="justify-content-around">{product}</Row>
			</Container>
		</Fragment>
	)
}

export default Bracelets