import {Fragment, useEffect, useState, useContext} from 'react'
import {Container} from 'react-bootstrap'
import {Route, Routes} from 'react-router-dom'
import AllProductCard from '../components/AllProductCard'
import PageNotFound from '../components/PageNotFound'
import UserContext from '../UserContext'

const AllEarrings = () => {
	const {user} = useContext(UserContext)

	const [product, setProducts] = useState([])

	useEffect(() => {
		fetch('https://ecommerce-ytmf.onrender.com/allEarrings', {
			headers : {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {
					return (
						<AllProductCard key={product._id} productProp={product} />
					)
				})
			)
		})
	}, [])

	return (
		(user.isAdmin === false || user.id === null) ?
			<Routes>
				<Route exact path="*" element={<PageNotFound />} />
			</Routes>
		:
			<Fragment>
				<Container className="mt-5" fluid>
					{product}
				</Container>
			</Fragment>
	)
}

export default AllEarrings