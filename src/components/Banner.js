import {Carousel, Container} from 'react-bootstrap'
import Banner1 from '../Images/Banner1.jpeg'
import Banner2 from '../Images/Banner2.jpeg'
import Banner3 from '../Images/Banner3.jpeg'

const Banner = () => {
	return (
		<Container className="mt-4 pt-3" fluid>
		<Carousel interval={2000} indicators={false} nextIcon={false} prevIcon={false} pause={false} fade>
			<Carousel.Item>
				<img
				className="d-block w-100"
				src={Banner1}
				alt="First slide"
				fluid="true"
				/>
			</Carousel.Item>

			<Carousel.Item>
				<img
				className="d-block w-100"
				src={Banner2}
				alt="Second slide"
				fluid="true"
				/>
			</Carousel.Item>
			
			<Carousel.Item>
				<img
				className="d-block w-100"
				src={Banner3}
				alt="Third slide"
				fluid="true"
				/>
			</Carousel.Item>
		</Carousel>
		</Container>
	)
}

export default Banner