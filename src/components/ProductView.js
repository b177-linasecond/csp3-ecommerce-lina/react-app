import React, {useState, useEffect, useContext} from 'react'
import {Container, Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import AddToCart from '../Images/AddToCart.png'

const ProductView = () => {
	const {user} = useContext(UserContext)
	const history = useNavigate()
	const {productId} = useParams()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState()
	const [quantity, setQuantity] = useState(0)
	const [image, setImage] = useState()
	const [count, setCount] = useState(1)

    const incrementCount = () => { 
    	if(count < quantity){
    		setCount(count + 1)
    	}
    	else {
    		setCount(count)
    	}     
    }

    const decrementCount = () => {  
        setCount((c) => Math.max(c - 1, 1));  
    }

	const checkout = (productId) => {
		fetch('https://ecommerce-ytmf.onrender.com/users/checkout', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body : JSON.stringify({
				productId : productId,
				name : name,
				description : description,
				price : price,
				quantity : count,
				image : image
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title : "Successfully purchased.",
					icon : "success"
				})
			}
			else {
				Swal.fire({
					title : "Purchase failed.",
					icon : "error"
				})
			}
			history('/products')
		})	
	}

	useEffect(() => {
		fetch(`https://ecommerce-ytmf.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setQuantity(data.quantity)
			setImage(data.image)
		})
	}, [productId])

	const numberFormat = (value) =>
		new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'PHP'
	}).format(value)

	return (
		<Container fluid>
			<Row className="justify-content-center align-content-center vh-100 m-0" >
			<Col xs={10} md={10}>
				<Card  className="justify-content-center">
					<Card.Body>
					<Row className="justify-content-around">
					<Col xs={5}>
						<Card.Img className="productImage" variant="top" src={image} fluid />
					</Col>
					<Col xs={5}>
				    	<Card.Title>
				    		{name}
				    	</Card.Title>

				    	<Card.Subtitle className="mb-2 text-muted">
				    		Description:
				    	</Card.Subtitle>

				    	<Card.Text className="text-wrap">
				    		{description}
				    	</Card.Text>

				    	<Card.Subtitle className="mb-2 text-muted">
				    		Price:
				    	</Card.Subtitle>

				    	<Card.Text>
				    		{numberFormat(price)}
				    	</Card.Text>

				    	<Card.Subtitle className="description">
				    		Stock:
				    	</Card.Subtitle>

				    	<Card.Text>
				    		{quantity}
				    	</Card.Text>

						<div className="mb-4">
							<button className='btn btn-dark btn-block' type="button" onClick={decrementCount} > - </button>  
								<span className='m-5'>{count} pc/s</span> 
							<button className='btn btn-dark btn-block' type="button" onClick={incrementCount} > + </button>
						</div>
				    	{
				    	(user.id !== null) ?
				    		<Button variant="primary" onClick={() => checkout(productId)}>
				    			<img className="icon" src={AddToCart} alt="horse" height="20px" />
				    			<p className="mb-0 textButton">Add to cart</p>
				    		</Button>
				    	:
				    		<Button variant="primary" as={Link} to={`/login`}>
				    			<img className="icon" src={AddToCart} alt="horse" height="20px" />
				    			<p className="mb-0 textButton">Login to add to cart</p>
				    		</Button>

				    	}
				    </Col>
				    </Row>
					</Card.Body>
				</Card>
				</Col>
			</Row>
		</Container>
	)
}

export default ProductView