import {Col, Card, Button, Row} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

export default function AllProductCard({productProp}) {
	const {_id, name, description, price, quantity, isActive, image, category} = productProp
	const numberFormat = (value) =>
		new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'PHP'
	}).format(value)

	const Capitalize = (str) => {
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	return (
			<Row className="mb-3 justify-content-around">
			<Col xs={11}>
				<Card className="card">
				  <Card.Body className="p-3 m-0">
				  <Row>
				  <Col>
				  	<Card.Img className="productImage" variant="top" src={image} fluid />
				  </Col>
				  <Col>
				    <Card.Title className="title m-0 p-0">
				    	{name}
				    </Card.Title>

				    <Card.Text>
				    </Card.Text>

				    <Card.Subtitle className="description">
				    	Description:
				    </Card.Subtitle>

				    <Card.Text>
				    	{description}
				    </Card.Text>

				    <Card.Subtitle className="description">
				    	Price:
				    </Card.Subtitle>

				    <Card.Text>
				    	{numberFormat(price)}
				    </Card.Text>

				    <Card.Subtitle className="description">
				    	Stock:
				    </Card.Subtitle>

				    <Card.Text>
				    	{quantity}
				    </Card.Text>

				    <Card.Subtitle className="description">
				    	On Sale:
				    </Card.Subtitle>

				    <Card.Text>
				    	{Capitalize(String(isActive))}
				    </Card.Text>

				    <Button variant="primary" as={Link} to={`/products/update/${_id}`}>Update</Button>
				   </Col>
				   </Row>
				  </Card.Body>
				</Card>
			</Col>
			</Row>

	)
}

AllProductCard.propTypes = {
	productProp : PropTypes.shape({
		name : PropTypes.string.isRequired,
		description : PropTypes.string.isRequired,
		price : PropTypes.number.isRequired,
		quantity : PropTypes.number.isRequired,
		isActive : PropTypes.bool.isRequired,
		category : PropTypes.string.isRequired
	})
}