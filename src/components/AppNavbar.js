import {Navbar, Nav, Container, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import React, {useContext} from 'react'
import UserContext from '../UserContext'
import Logo from '../Images/Logo.png'
import Login from '../Images/Login.png'
import Cart from '../Images/Cart.png'
import Logout from '../Images/Logout.png'

const AppNavbar = () => {
	const {user} = useContext(UserContext)

	return (
		<Navbar variant="light" expand="lg" fixed="top">
			<Container className="pl-5" fluid>
				<Row className="vw-100 justify-content-around">
					<Col xs={{order: 'send'}} style={{display:'flex', justifyContent:'center'}}>
						<Nav  >
							<Nav.Link as={Link} to="/">
								<img src={Logo} alt="Logo" height="30px" />
							</Nav.Link>
						</Nav>
					</Col>

					<Col xs={{order: 'last'}} style={{display:'flex', justifyContent:'right'}}>
						<Row>
							<Col>
								<Nav>
									{(user.isAdmin === false && user.id !== null) ?
										<React.Fragment>
											<Nav.Link className="iconzoom" as={Link} to="/orders">
												<img className="icon" src={Cart} alt="Orders" height="15px" />
											</Nav.Link>
										</React.Fragment>
										:
										null
									}
								</Nav>
							</Col>
							<Col>
								<Nav>
									{(user.id === null) ?
										<Nav.Link className="iconzoom" as={Link} to="/login">
											<img className="icon" src={Login} alt="Login" height="15px" />
										</Nav.Link>
									:
										<Nav.Link className="iconzoom" as={Link} to="/logout">
											<img className="icon3" src={Logout} alt="Logout" height="15px" />
										</Nav.Link>
									}
								</Nav>
							</Col>
						</Row>
					</Col>

					<Col xs={{order: 'first'}} >
						<Navbar.Toggle aria-controls="basic-navbar-nav" />
					    <Navbar.Collapse id="basic-navbar-nav" className="mt-1 mb-1">
							<Nav className="me-auto">
							{(user.id === null || (user.id !== null && user.isAdmin === false)) ?
								<React.Fragment>
									<Nav.Link as={Link} to="/products">
										Products
									</Nav.Link>
									<Nav.Link as={Link} to="/products/rings">
										Rings
									</Nav.Link>
									<Nav.Link as={Link} to="/products/necklaces">
										Necklaces
									</Nav.Link>
									<Nav.Link as={Link} to="/products/bracelets">
										Bracelets
									</Nav.Link>
									<Nav.Link as={Link} to="/products/earrings">
										Earrings
									</Nav.Link>
								</React.Fragment>
								:
								null
							}
							
							{(user.isAdmin === true) ?
								<React.Fragment>
									<Nav.Link as={Link} to="/products/all">
										Products
									</Nav.Link>

									<Nav.Link as={Link} to="/products/allRings">
										Rings
									</Nav.Link>

									<Nav.Link as={Link} to="/products/allNecklaces">
										Necklaces
									</Nav.Link>

									<Nav.Link as={Link} to="/products/allBracelets">
										Bracelets
									</Nav.Link>

									<Nav.Link as={Link} to="/products/allEarrings">
										Earrings
									</Nav.Link>

									<Nav.Link as={Link} to="/products/create">
										Add Products
									</Nav.Link>

									<Nav.Link as={Link} to="/orders/all">
										Orders
									</Nav.Link>
								</React.Fragment>
								:
								null
							}	
							</Nav>
						</Navbar.Collapse>
					</Col>
				</Row>
			</Container>
		</Navbar>
	)
}

export default AppNavbar