import {Container, Row} from 'react-bootstrap'

const Footer = () => (
  <Container className="footer" fluid>
  	<Row className="justify-content-center">
    	℗ Jewels by J 2022
    </Row>
  </Container>
)

export default Footer;