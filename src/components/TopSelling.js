import {Col, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

export default function TopSelling({productProp}) {
	const {_id, image} = productProp

	return (
		<Col className="m-3" xs={5} md={2}>

			<Button variant="danger" as={Link} to={`/products/${_id}`}>

				<img className="topProducts" src={image} alt="item" />
				
			</Button>

		</Col>
	)
}

TopSelling.propTypes = {
	productProp : PropTypes.shape({
		name : PropTypes.string.isRequired
	})
}