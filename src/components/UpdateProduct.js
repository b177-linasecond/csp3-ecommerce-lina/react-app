import {useState, useEffect, useContext} from 'react'
import {Container, Button, Row, Col, Form} from 'react-bootstrap'
import {useParams, useNavigate, Route, Routes} from 'react-router-dom'
import PageNotFound from '../components/PageNotFound'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

const ProductView = () => {
	const {user} = useContext(UserContext)
	const history = useNavigate()
	const {productId} = useParams()

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [quantity, setQuantity] = useState(0)
	const [isActive, setIsActive] = useState()
	const [image, setImage] = useState()

	const update = (e) => {
		e.preventDefault()		
		fetch(`https://ecommerce-ytmf.onrender.com/products/${productId}`, {
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body : JSON.stringify({
				name : name,
				description : description,
				price : price,
				quantity : quantity,
				image : image
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title : "Product is updated.",
					icon : "success"
				})
			}
			else {
				Swal.fire({
					title : "Something went wrong!",
					icon : "error"
				})
			}
		})

		history('/products/all')
	}

	const available = e => {
		e.preventDefault()
		fetch(`https://ecommerce-ytmf.onrender.com/products/${productId}/archive`, {
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => data)

		if(isActive === false) {
			Swal.fire({
					title : "Product is now on sale",
					icon : "success"
			})
		}
		else {
			Swal.fire({
					title : "Product is now archived",
					icon : "success"
			})

		}
		
		history(`/products/update/${productId}`)
	}
	

	useEffect(() => {
		fetch(`https://ecommerce-ytmf.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data.isActive)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setQuantity(data.quantity)
			setIsActive(data.isActive)
			setImage(data.image)
		})
	}, [productId])

	return (
		(user.isAdmin === false || user.id === null) ?
			<Routes>
				<Route exact path="*" element={<PageNotFound />} />
			</Routes>
		:
			<Container fluid>
				<Row className="justify-content-center align-items-center vh-100 m-0 p-0">
					<Col xs="10" md="4" align="center">
						<Form onSubmit={e => update(e)}>
							<Form.Group className="mb-3" controlId="productImage">
								<h6>Photos</h6>

								<Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userFName">
								<h6>Product Name</h6>

								<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userLName">
								<h6>Description</h6>

								<Form.Control as="textarea" rows={3} type="text" value={description} onChange={e => setDescription(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userEmail">
								<h6>Price</h6>

								<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userEmail">
								<h6>Stock</h6>

								<Form.Control type="number" value={quantity} onChange={e => setQuantity(e.target.value)} required />
							</Form.Group>

							<Form.Group className="mb-3" controlId="userEmail">
								<h6>On Sale:</h6>

									<Button as="input" value={!isActive} variant="primary" type="submit" id="submitBtn" onClick={(e) => {available(e); setIsActive(e.target.value)}} />
							</Form.Group>

							<Button variant="primary" type="submit" id="submitBtn">
								Update
							</Button>
						</Form>
					</Col>
				</Row>
			</Container>
	)
}

export default ProductView