import {Col, Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}) {
	const {_id, image} = productProp

	return (
		<Col className="m-3" xs={5} md={3}>
			<Card className="productCardUser">
				<Card.Body className="p-0 m-0">
					<Button variant="danger" as={Link} to={`/products/${_id}`}>
						<img className="products" src={image} alt="item" />
					</Button>
				</Card.Body>
			</Card>
		</Col>
	)
}