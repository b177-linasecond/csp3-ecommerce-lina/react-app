import {Col, Card, Row} from 'react-bootstrap'
import PropTypes from 'prop-types'

export default function OrderCard({orderProp}) {
	const {name, totalAmount, quantity, purchasedOn, image} = orderProp

	const numberFormat = (value) =>
		new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'PHP'
	}).format(value)

	return (
		<Col className="m-3" xs={11} md={11}>
			<Card className="productCard" >
			  <Card.Body className="p-3 m-0">
			  <Row className="justify-content-around">
			  <Col xs={5}>
			  	<Card.Img className="productImage" variant="top" src={image} fluid />
			  </Col>
			  <Col xs={5}>
			    <Card.Title className="title m-0 p-0">
			    	{name}
			    </Card.Title>

			    <Card.Text>
			    </Card.Text>

			    <Card.Subtitle className="description">
			    	Price:
			    </Card.Subtitle>

			    <Card.Text>
			    	{numberFormat(totalAmount)}
			    </Card.Text>

			    <Card.Subtitle className="description">
			    	Quantity:
			    </Card.Subtitle>

			    <Card.Text>
			    	{quantity}
			    </Card.Text>

			    <Card.Subtitle className="description">
			    	Date Purchased:
			    </Card.Subtitle>

			    <Card.Text>
			    	{purchasedOn}
			    </Card.Text>
			  </Col>
			  </Row>
			  </Card.Body>
			</Card>
		</Col>
	)
}

OrderCard.propTypes = {
	orderProp : PropTypes.shape({
		name : PropTypes.string.isRequired,
		totalAmount : PropTypes.number.isRequired,
		quantity : PropTypes.number.isRequired
})
}